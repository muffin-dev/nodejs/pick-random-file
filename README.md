# Muffin Dev for Node - Pick Random File

This utility picks a random file in a directory, and displays the path to that picked file.

## Installation

Installit using npm by running the following command:

```bash
npm i -g @muffin-dev/pick-random-file
```

## Usage

Once installed, run `pick-random-file` (or its shortcut `rndfile`). By default, it uses the current directory (equivalent to type `./`).

You can also write file extensions to filter files by type.

Examples:

```bash
pick-random-file
pick-random-file /path/to/directory
pick-random-file /path/to/directory wav
pick-random-file /path/to/directory png jpg gif
```