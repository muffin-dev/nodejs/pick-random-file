#!/usr/bin/env node
import pickRandomFile from '../';
import { IPickRandomFileResult } from '../lib';

// process.argv[2] should be the directory path
const directory = process.argv.length >= 3 ? process.argv[2] : './';

// Other datas are the expected file extensions
let extensions = null;
if(process.argv.length > 3) {
  extensions = process.argv.slice(3);
}

(async () => {

  const result: IPickRandomFileResult = await pickRandomFile(directory, { extensions, full: true }) as IPickRandomFileResult;
  if(result.found) {
    console.log('Picked file: ' + result.path);
  }
  else {
    console.error('Error: ' + result.errorMessage, result);
    process.exit(-1);
  }

})();
