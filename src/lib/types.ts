export type TPickRandomFileErrorType =

  // The directory doesn't exist
  'ENOENT' |

  // Empty directory
  'EEMPTY' |

  // No file with the given extension(s) in the directory
  'EEXTS' |

  // Permission denied for one of the file or directory
  'EPERM';
