import { IPickRandomFileOptions } from './interfaces/options';

/**
 * Defines the default options.
 */
export const DEFAULT_OPTIONS: IPickRandomFileOptions = {
  extensions: null,
  full: false
};
