/**
 * @interface IPickRandomFileOptions Represents the user options.
 */
export interface IPickRandomFileOptions {
  /**
   * @property The extension of list of extensions of the files to find.
   */
  extensions?: string|string[];

  /**
   * @property If disabled, returns the path to the found file. If enabled, returns the full data about the found file.
   */
  full?: boolean;
}