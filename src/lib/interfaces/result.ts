import { IDirItem } from '@muffin-dev/js-helpers';
import { TPickRandomFileErrorType } from '../types';

/**
 * @interface IPickRandomFileResult Represents the informations of a randomly picked file.
 */
export interface IPickRandomFileResult {
  /**
   * @property True if the file has been found, otherwise false.
   */
  found: boolean;

  /**
   * @property The absolute path to the picked file.
   */
  path: string;

  /**
   * @property The complete informations about the picked file.
   */
  fileInfos: IDirItem;

  /**
   * @property The identifier of the eventual error that has occured.
   */
  errorCode: TPickRandomFileErrorType;

  /**
   * @property The default message of the eventual error that has occured.
   */
  errorMessage: string;

  /**
   * @property The original thrown error. Only used for system error (like permission denied, misisng directory, etc).
   */
  error: any;
}