import { TPickRandomFileErrorType } from './types';

/**
 * @interface IPickRandomFileError Represents an error thrown by pickRandomFile() method.
 */
interface IPickRandomFileError {
  code: TPickRandomFileErrorType;
  message: string;
}

/**
 * Defines the default messages to use for the common errors.
 */
const ERROR_MESSAGES: IPickRandomFileError[] = [
  { code: 'ENOENT', message: 'This directory doesn\'t exist' },
  { code: 'EEMPTY', message: 'This directory is empty' },
  { code: 'EEXTS', message: 'No file with the given extension(s) in the directory' },
  { code: 'EPERM', message: 'You don\'t have the access permissions to a file or a sub-directory' }
];

/**
 * Defines the default message for unknown errors.
 */
const UNKNOWN_ERROR_MESSAGE = 'Unknown error';

/**
 * Gets the default message of an error thrown by pickRandomFile() method.
 * @param errorCode The thrown error code.
 */
function getErrorMessage(errorCode: string) {
  for(const err of ERROR_MESSAGES) {
    if(errorCode === err.code) {
      return err.message;
    }
  }
  return UNKNOWN_ERROR_MESSAGE;
}

export default getErrorMessage;
