import { isAbsolute, join } from 'path';

import { IDirItem, JSHelpers, NodeHelpers } from '@muffin-dev/js-helpers';
import { randomInteger } from '@muffin-dev/maths';

import { IPickRandomFileOptions } from './interfaces/options';
import { DEFAULT_OPTIONS } from './constants';
import getErrorMessage from './errors';
import { IPickRandomFileResult } from './interfaces';
import { TPickRandomFileErrorType } from './types';

//#region Utility methods

/**
 * Makes an object that matches the IPickRandomFileOptions interface, based on the user options.
 * @param options The user options object.
 * @returns Returns the validated options object.
 */
function makePickRandomFileOptions(options: string|string[]|IPickRandomFileOptions = null): IPickRandomFileOptions {
  const opts = Object.assign({ }, DEFAULT_OPTIONS);
  if(options === null) return opts;

  // If the given options object is a file extension or a list of file extensions
  if(typeof options === 'string' || Array.isArray(options)) {
    opts.extensions = options;
  }
  // if the given options object is an actual options object
  else if(options !== null) {
    JSHelpers.overrideObject(opts, options);
  }

  // If the optional extensions is a single file extension string, convert it as an array
  if(typeof opts.extensions === 'string') {
    opts.extensions = new Array<string>(opts.extensions);
  }
  // If the optional extensions data is not valid
  else if(opts.extensions !== null && !Array.isArray(opts.extensions)) {
    opts.extensions = null;
  }

  return opts;
}

//#endregion

//#region Public API

/**
 * Picks a file randomly in a given directory.
 * @param path The path to the directory that contains the files to pick.
 * @param options The options to use.
 * @returns Returns the path to the found file, or a IPickRandomFileResult object if the "full" option is enabled.
 */
export async function pickRandomFile(
  path: string,
  options: string|string[]|IPickRandomFileOptions = null
): Promise<string|IPickRandomFileResult> {
  // Fix path
  if(!isAbsolute(path)) {
    path = join(process.cwd(), path);
  }

  options = makePickRandomFileOptions(options);

  // Search for files
  let files: IDirItem[] = null;
  try {
    files = await NodeHelpers.readdirAsync(path, false, true, options.extensions, false, true);
  }
  catch(error) {
    if(options.full) {
      const errorMessage = getErrorMessage(error ? error.code : null);
      const errorCode = error ? error.code || null : null;
      const result: IPickRandomFileResult = { found: false, fileInfos: null, path: null, errorCode, errorMessage, error };
      return result;
    }
    else {
      throw error;
    }
  }

  // Check for directory
  if(files.length === 0) {
    const errorCode: TPickRandomFileErrorType = (options.extensions && options.extensions.length > 0) ? 'EEXTS' : 'EEMPTY';
    const errorMessage = getErrorMessage(errorCode);

    if(options.full) {
      const result: IPickRandomFileResult = { found: false, fileInfos: null, path: null, errorCode, errorMessage, error: null };
    }
    throw new Error(errorMessage);
  }

  // Pick random file
  const rnd = randomInteger(0, files.length);
  return options.full
    ? { found: true, fileInfos: files[rnd], path: files[rnd].path, errorCode: null, errorMessage: null, error: null }
    : files[rnd].path;
}

//#endregion

export default pickRandomFile;
export * from './interfaces';
export * from './types';